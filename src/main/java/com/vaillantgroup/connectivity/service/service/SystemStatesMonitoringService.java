package com.vaillantgroup.connectivity.service.service;

import com.ecwid.consul.v1.ConsistencyMode;
import com.ecwid.consul.v1.ConsulClient;
import com.ecwid.consul.v1.QueryParams;
import com.ecwid.consul.v1.Response;
import com.ecwid.consul.v1.health.model.Check;
import com.ecwid.consul.v1.health.model.HealthService;
import com.vaillantgroup.connectivity.service.api.monitoring.SystemState;
import com.vaillantgroup.connectivity.service.discovery.Dependency;
import com.vaillantgroup.connectivity.service.discovery.ServiceDeploymentDescriptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.consul.discovery.ConsulDiscoveryClient;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class SystemStatesMonitoringService {
    @Autowired
    private ConsulClient consulClient;

    @Autowired
    private ServiceDeploymentDescriptor serviceDeploymentDescriptor;


    public List<SystemState> listSystemStates() {
        return getSystemStates();
    }

    protected List<SystemState> getSystemStates() {
        return serviceDeploymentDescriptor.getService().getDependencies()
                .stream()
                .map( dependency -> getSystemState(dependency)).collect(Collectors.toList());
    }

    private SystemState getSystemState(Dependency dependency) {
        Response<List<HealthService>> services = getHealthInformation(dependency, ConsistencyMode.DEFAULT);
        SystemState systemState = SystemState.UNKNOWN;

        systemState.setService(dependency.getServiceId());

        for(HealthService service: services.getValue()) {
            List<Check> checks = service.getChecks();

            for(Check check : checks) {
                SystemState.Status status = SystemState.Status.map(check.getStatus());

                if(systemState.getStatusValue().compare(status) < -1) {
                    systemState.setStatus(status);
                }
            }
        }

        if(systemState.getStatusValue().equals(SystemState.Status.UNKNOWN)) {
            if(!isRegisteredService(dependency)) {
                systemState.setStatus(SystemState.Status.NO_SERVICE_DISCOVERY);
            }
        }

        return systemState;
    }

    private Response<List<HealthService>> getHealthInformation(Dependency dependency, ConsistencyMode consistencyMode) {
        Response<List<HealthService>> services = consulClient.getHealthServices(dependency.getServiceId(),
                false, new QueryParams(consistencyMode));

        return services;
    }

    private boolean isRegisteredService(Dependency dependency) {
        Response<Map<String, List<String>>> serviceInstances = getAllRegisteredInstances();

        for (String serviceId : serviceInstances.getValue().keySet()) {
            if(dependency.getServiceId().equals(serviceId)) {
                return true;
            }
        }

        return false;
    }

    private Response<Map<String, List<String>>> getAllRegisteredInstances() {
        return consulClient.getCatalogServices(QueryParams.DEFAULT);
    }

}
