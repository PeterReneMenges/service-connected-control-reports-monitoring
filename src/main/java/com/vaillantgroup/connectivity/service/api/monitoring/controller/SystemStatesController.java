package com.vaillantgroup.connectivity.service.api.monitoring.controller;

import com.vaillantgroup.connectivity.service.api.monitoring.SystemState;
import com.vaillantgroup.connectivity.service.service.SystemStatesMonitoringService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@RequestMapping(value = "/reports-monitoring", produces = APPLICATION_JSON_VALUE)
public class SystemStatesController {
    private final @NotNull SystemStatesMonitoringService systemStatesMonitoringService;

    @Autowired
    public SystemStatesController(final @NotNull SystemStatesMonitoringService systemStatesMonitoringService) {
        this.systemStatesMonitoringService = systemStatesMonitoringService;
    }

    @RequestMapping(value = "systems", method = {GET})
    public List<SystemState> listSystemStates() {
        return systemStatesMonitoringService.listSystemStates();
    }
}
