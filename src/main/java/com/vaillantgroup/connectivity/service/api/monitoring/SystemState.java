package com.vaillantgroup.connectivity.service.api.monitoring;


import com.ecwid.consul.v1.health.model.Check;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class SystemState {
    public static final SystemState UNKNOWN = new SystemState();
    private String service;
    private Status status;


    /**
     *  public static enum CheckStatus {
    @SerializedName("unknown")
    UNKNOWN,
    @SerializedName("passing")
    PASSING,
    @SerializedName("warning")
    WARNING,
    @SerializedName("critical")
    CRITICAL;

    private CheckStatus() {
    }
    }
     */
    public enum Status {
        NO_SERVICE_DISCOVERY("serviceDiscoveryFailure"),
        UNKNOWN("unknown"),
        PASSING("passing"),
        WARNING("warning"),
        CRITICAL("critical");

        private String name;

        Status(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public int compare(Status other) {
            if(other.equals(this))
                return 0;
            if(other.equals(CRITICAL))
                return -1;
            if(this.equals(CRITICAL))
                return 1;
            if(other.equals(WARNING))
                return -1;
            if(this.equals(WARNING))
                return 1;
            if(other.equals(PASSING))
                return -1;
            if(this.equals(PASSING))
                return 1;
            return -1;
        }

        static public Status map(Check.CheckStatus checkStatus) {
            switch(checkStatus) {
                case UNKNOWN:
                    return Status.UNKNOWN;
                case PASSING:
                    return Status.PASSING;
                case WARNING:
                    return Status.WARNING;
                case CRITICAL:
                    return Status.CRITICAL;
                default:
                    return Status.UNKNOWN;
            }
        }
    }

    @JsonCreator
    public SystemState(@JsonProperty("service") String service, @JsonProperty("status") Status status) {
        this.service = service;
        this.status = status;
    }

    public SystemState() {
        this.status = Status.NO_SERVICE_DISCOVERY;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @JsonIgnore
    public Status getStatusValue() {
        return this.status;
    }

    @JsonProperty("status")
    public String getStatus() {
        return this.status.getName();
    }
}
