package com.vaillantgroup.connectivity.service.configuration;


import com.vaillantgroup.connectivity.service.discovery.ServiceDeploymentDescriptor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties
public class ClientConfiguration {
    private ServiceDeploymentDescriptor serviceDeploymentDescriptor;

    public ServiceDeploymentDescriptor getServiceDeploymentDescriptor() {
        return serviceDeploymentDescriptor;
    }

    public void setServiceDeploymentDescriptor(ServiceDeploymentDescriptor serviceDeploymentDescriptor) {
        this.serviceDeploymentDescriptor = serviceDeploymentDescriptor;
    }

    @Bean
    public ServiceDeploymentDescriptor serviceDeploymentDescriptor() {
        return this.serviceDeploymentDescriptor;
    }
}
