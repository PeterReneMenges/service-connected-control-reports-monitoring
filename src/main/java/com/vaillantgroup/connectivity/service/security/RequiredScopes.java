package com.vaillantgroup.connectivity.service.security;

public interface RequiredScopes {
    String REPORTS_MONITORING_READ = "REPORTS-MONITORING-READ";
}
