package com.vaillantgroup.connectivity.service.discovery;

import java.util.List;

public class Service {
    private String id;
    private List<Dependency> dependencies;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Dependency> getDependencies() {
        return dependencies;
    }

    public void setDependencies(List<Dependency> dependencies) {
        this.dependencies = dependencies;
    }
}
