package com.vaillantgroup.connectivity.service.discovery;

public class Dependency {
    private String serviceId;

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }
}
