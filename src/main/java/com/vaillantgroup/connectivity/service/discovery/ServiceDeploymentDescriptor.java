package com.vaillantgroup.connectivity.service.discovery;

import java.util.List;

public class ServiceDeploymentDescriptor {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
