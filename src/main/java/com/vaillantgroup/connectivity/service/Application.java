package com.vaillantgroup.connectivity.service;

import com.vaillantgroup.connectivity.service.configuration.ClientConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

import static com.vaillantgroup.connectivity.service.security.SslUtils.disableCertificateValidation;


@VaillantGroupService
@ComponentScan
public class Application {
	public static void main(final String... args) {
		disableCertificateValidation();
		SpringApplication.run(Application.class, args);
	}
}
