package com.vaillantgroup.connectivity.service;

import com.vaillantgroup.connectivity.service.configuration.ClientConfiguration;
import com.vaillantgroup.connectivity.starter.test.functional.FunctionalTest;
import com.vaillantgroup.connectivity.starter.test.functional.mock.annotation.ServiceMock;
import com.vaillantgroup.connectivity.starter.test.rest.RestClient;
import com.vaillantgroup.connectivity.starter.test.rest.result.Result;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.testng.annotations.Test;

import static com.vaillantgroup.connectivity.starter.test.rest.RestClient.perform;
import static com.vaillantgroup.connectivity.starter.test.rest.RestClient.status;
import static org.springframework.http.HttpStatus.OK;

@ServiceMock("management-zuul")
@ServiceMock("consul")
@SpringApplicationConfiguration(classes = {
        Application.class,
        ClientConfiguration.class
})
public class ReportsMonitoringFunctionalTest extends FunctionalTest {
    @Test
    public void testListSystemStates() throws Exception {
        this.addStubMappings("management-zuul", "userInfo.json", "tokenInfo.json");
        this.addStubMappings("consul", "healthNode.json",  "healthService.json");

        Result result = perform(
                RestClient
                        .get(this.getLocalServiceUrl("/reports-monitoring/systems"))
                        .header(HttpHeaders.AUTHORIZATION, "Bearer ZmFrZTpmYWtl")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(OK))
                .andReturn();
    }
}
